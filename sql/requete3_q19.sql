SELECT
	equilibre_prod.Pays, 
	sum(equilibre_prod.Pertes_millier_tonnes*1000*1000) as total_perte,
	equilibre_prod.Année
FROM equilibre_prod
GROUP BY
	equilibre_prod.Pays , equilibre_prod.Année
ORDER BY 
equilibre_prod.Pays , equilibre_prod.Année


