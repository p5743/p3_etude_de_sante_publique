SELECT
	dispo_alim.Pays, 
	sum(dispo_alim.Disponibilitédeprotéinesenquantité*0.001*365) as total_dispo_prot,
	dispo_alim.Année
FROM dispo_alim
WHERE dispo_alim.Année=2013
GROUP BY
	dispo_alim.Pays
ORDER BY total_dispo_prot DESC
LIMIT 10;