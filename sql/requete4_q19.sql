SELECT population_sous_nutri.Pays, population_sous_nutri / total_population as ratio_sousnutri FROM
(
  SELECT nb_personnes*1000000 AS population_sous_nutri, sous_nutrition.Pays
  FROM sous_nutrition
  WHERE sous_nutrition.Année="2015-2017"
) population_sous_nutri

INNER JOIN 
(
  SELECT Population AS total_population, Pays
  FROM population 
  WHERE Année=2017
) total_population
ON population_sous_nutri.Pays = total_population.Pays

ORDER BY ratio_sousnutri DESC
LIMIT 10