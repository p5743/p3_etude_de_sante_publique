select Produit, AVG(tmp2.moy1_ratio_autre_inter) as avg_ratio_autre_inter from
(
select Produit, Année, AVG(tmp.ratio_autre_inter) as moy1_ratio_autre_inter from 
(
SELECT
	equilibre_prod.Produit,
	equilibre_prod.AutresUtilisations/equilibre_prod.Disponibilitéintérieure AS ratio_autre_inter,
	equilibre_prod.Année
FROM equilibre_prod
)tmp
GROUP BY tmp.Année, tmp.Produit
)tmp2
group by tmp2.Produit
ORDER BY avg_ratio_autre_inter DESC
LIMIT 10

 
