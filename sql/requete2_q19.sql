select * from 
(
SELECT 
	 dispo_alim.Pays ,
	 dispo_alim.Année,
	 sum(dispo_alim.Disponibilitédeprotéinesenquantité*0.001*365) as total_dispo_prot
FROM dispo_alim
WHERE dispo_alim.Année= 2012
GROUP BY dispo_alim.Pays
ORDER BY total_dispo_prot ASC
LIMIT 10
)
UNION ALL 

select * from 
(
SELECT 
	 dispo_alim.Pays ,
	 dispo_alim.Année,
	 sum(dispo_alim.Disponibilitédeprotéinesenquantité*0.001*365) as total_dispo_prot
FROM dispo_alim
WHERE dispo_alim.Année= 2013
GROUP BY dispo_alim.Pays
ORDER BY total_dispo_prot ASC
LIMIT 10
)