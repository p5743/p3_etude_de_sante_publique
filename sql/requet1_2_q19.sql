SELECT dispo_alim.Pays,
sum(dispo_alim.Disponibilitéalimentairekcal*365) AS ratio_disponibilité_alimentaire_habitant_kcal,
dispo_alim.Année
FROM dispo_alim
WHERE dispo_alim.Année=2013
group by dispo_alim.Pays
ORDER BY ratio_disponibilité_alimentaire_habitant_kcal DESC
LIMIT 10
